#!/usr/bin/python
Carre=[]
ligne=[] 
for j in range(26):
    for i in range(26):
        ligne.append(chr(65+(i+j)%26)) 
    Carre.append(ligne)
    ligne=[]
    
print (""" 
╔═╗╔═╗╔═══╗╔════╗╔════╗╔╗─╔╗╔═══╗──────     ╔═══╗╔═══╗╔╗──╔╗╔═══╗╔═══╗
║║╚╝║║║╔═╗║║╔╗╔╗║║╔╗╔╗║║║─║║║╔══╝──────     ║╔═╗║║╔═╗║║╚╗╔╝║║╔═╗║║╔══╝
║╔╗╔╗║║║─║║╚╝║║╚╝╚╝║║╚╝║╚═╝║║╚══╗╔╗╔╗╔╗     ║╚═╝║║╚═╝║╚╗╚╝╔╝║║─╚╝║╚══╗
║║║║║║║╚═╝║──║║────║║──║╔═╗║║╔══╝║╚╝╚╝║     ║╔══╝║╔╗╔╝─╚╗╔╝─║║─╔╗║╔══╝
║║║║║║║╔═╗║──║║────║║──║║─║║║╚══╗╚╗╔╗╔╝     ║║───║║║╚╗──║║──║╚═╝║║╚══╗
╚╝╚╝╚╝╚╝─╚╝──╚╝────╚╝──╚╝─╚╝╚═══╝─╚╝╚╝─     ╚╝───╚╝╚═╝──╚╝──╚═══╝╚═══╝ 
\n""")    
 
   
TexteClair=input("Digite o texto (em maiúscula):") 
lg_tc=len(TexteClair)
Cle=input ("Digite a chave (em letras maiúsculas):") 
lg_C=len(Cle)
bl=0                  
TexteCrypte=""
 
# Cryptage
for i in range(lg_tc):
    lettre=TexteClair[i]
    if lettre==" ":
        bl+=1
        TexteCrypte+=" "
    else:
        j=(i-bl)%lg_C           # % sert a inserer les valeurs dans la chaine
        k=ord(Cle[j])-65        #ord sert a donner le chiffre correspondant a la lettre dans la table ASCII
        h=ord(lettre)-65
        TexteCrypte+= Carre[h][k]
 
# Affichages
print("Texto original :",TexteClair)
print ("chave :",Cle)
print ("Texto criptografado :",TexteCrypte)

